export class HeapNode {

	constructor( item, value ) {
		this.item = item;
		this.value = value;
	}

}

export default class Heap {

	get count() { return this.heap.length; }

	constructor() {

		this.heap = [];

		// Maximum index for which this.heap[ind] is valid.
		// i.e. this.heap.length-1
		this.MAX_IND = -1;

	}

	add( item, value ) {

		let insert = ++this.MAX_IND;
		let parent;
		
		let pInd = this._pIndex( insert );

		while ( pInd >= 0 ) {

			parent = this.heap[ pInd ];
			if ( parent.value <= value ) break;

			// move parent down.
			this.heap[insert] = parent;

			insert = pInd;
			pInd = this._pIndex( pInd );

		}

		this.heap[insert] = new HeapNode( item, value );

	}

	peekMin() { return this.MAX_IND < 0 ? undefined : this.heap[0].item; }

	getMin() {

		if ( this.MAX_IND < 0 ) return undefined;

		let res = this.heap[0].item;

		let child, node = this.heap.pop();

		if ( --this.MAX_IND < 0 ) return res;

		let value = node.value;
		let cInd, ind = 0;

		for( cInd = this._minChild(0); cInd >= 0; cInd = this._minChild(ind) ) {

			child = this.heap[cInd];
			if ( child.value >= value ) break;

			this.heap[ind] = child;
			ind = cInd;

		}

		this.heap[ind] = node;

		return res;

	}

	/**
	 * Returns the index of the child node with the smallest value.
	 * Returns -1, if no child exists.
	 * @param {number} ind - index of parent node.
	 * @returns {number} index of child with smallest value, or -1 if no child exists.
	 */
	_minChild(ind) {

		ind = (ind<<2)+1;
		if ( ind > this.MAX_IND ) return -1;
		// only one child.
		if ( ind === this.MAX_IND ) return ind;
		this.heap[ind].value <= this.heap[ind+1].value ? ind : ind+1;

	}

	/**
	 * Get the parent node index for a given child node index.
	 * @param {number} ind - index of the child node.
	 * @returns {number} index of parent node.
	 */
	_pIndex( ind ) { return Math.floor( (ind-1)/2); }

	/**
	 * Get the index of the first child of a node stored at ind.
	 * @param {number} ind - index of the node.
	 * @returns {number} index of the first child.
	 */
	_cIndex( ind ) { return (ind<<2) + 1; }

}