
export default class Surface {

	get canvas() { return this._canvas; }
	set canvas(v) { this._canvas = v; }

	get context() {return this._context; }

	constructor( width, height, alpha=false ) {

		this.canvas = document.createElement('canvas');
		if ( !canvas) return console.error( 'Render missing canvas.');

		this._canvas.width = width;
		this._canvas.height = height;

		this._context = this._canvas.getContext( '2d', {alpha:alpha} );


	}


}