import Color from "./color";

var MY_ID=0;

export default class ColorSet {

	get colors() { return this._colors; }

	/**
	 * {Point|null} - location in image where the ColorSet started ( for contiguous ColorSets. )
	 */
	get loc() { return this._loc; }

	/**
	 * {Number|Color} The color to compare other colors to, before inclusion in the set.
	 */
	get baseColor() { return this._baseColor; }

	/**
	 * {Color} - the representative color that will represent all other colors
	 * in this ColorSet. If no value has been set, baseColor will be used.
	 */
	get color() { return this._color || this._baseColor; }
	/**
	 * {Color} set the representative color that will stand for all colors
	 * in this colorSet.
	 */
	set color(v) { this._color = v; }

	/**
	 * Number of pixels placed in the color set.
	 */
	get pixelCount() { return this._pixelCount; }

	/**
	 * Number of distinct colors in the set.
	 */
	get colorCount() { return this._colorCount; }

	get id() { return this._id; }

	/**
	 * 
	 * @param {Number|Color} baseColor - The color to compare other colors to, before inclusion in the set.
	 * @param {Point|null} loc - The point of the image where the baseColor started ( for contiguous color sets. )
	 */
	constructor( baseColor, loc=null ){

		this._colors = {};

		this._id = MY_ID++;
	
		if ( typeof(baseColor) === 'string' ) {

		} else if ( !isNaN(baseColor )) {

			var num = Number(baseColor);
			baseColor = Color.ParseRGB( baseColor );
			 
			this._colors[ num ] = { color:baseColor, count:1 };

		} else if ( baseColor instanceof Color ) {

			this._colors[ baseColor.toRGB() ] = { color:baseColor, count:1 };
		}

		//console.log('NEW COLOR SET: ' + baseColor.toCSS() );

		this._baseColor = baseColor;

		this._pixelCount = 1;

		this._loc = loc;


	}

	/**
	 * Returns the absolute color difference between this set and another colorSet.
	 * @param {ColorSet} colorSet 
	 */
	getDiff( colorSet ) {
		return colorSet.color.absDiff( this.color );
	}

	/**
	 * Merge the colors in a second color set with this one.
	 * This color set retains its base color, but average colors
	 * must be recomputed.
	 * @param {*} colorSet 
	 */
	merge( colorSet ) {

		this._pixelCount += colorSet._pixelCount;

		let otherColors = colorSet.colors;
		var cur;
		for( let n in otherColors ) {

			cur = this._colors[n];
			if ( !cur ) {

				this._colors[n] = otherColors[n];

			} else cur.count += otherColors[n].count;

		}

	}

	dist( other ) { return this._color.dist( other._color); }

	/**
	 * Tests if the set contains the given color.
	 * @param {Number} n
	 */
	contains( n ) { return this._colors[n] != null; }

		/**
	 * @param {Color} color - color object to add to set.
	 */
	addColor( color ) {

		this._pixelCount++;

		let n = color.toRGB();
		let o = this._colors[ n ];
		if ( !o ) {

			this._colors[n] = { color:color, count:1 };

		} else ++o.count;


	}

	/**
	 * @param {Number} n - color as an RGB integer.
	 */
	addIntColor( n ) {

		this._pixelCount++;

		let o = this._colors[n];
		if ( !o ) {

			this._colors[n] = { color:Color.ParseRGB(n), count:1 };

		} else o.count++;


	}

	/**
	 * Currently unused.
	 * @param {*} n 
	 * @param {*} testFunc 
	 */
	tryAdd( n, testFunc ) {

		let o = this._colors[n];
		this._pixelCount++;
		if ( o ) {
			
			/// color is already in the set, so must be a valid inclusion.
			o.count++;
			return true;

		} else if ( testFunc(this.baseColor, n ) ) {

			// color did not exist in set, but passed the testing function.
			this._colors[n] = o = { color:Color.ParseRGB(n), count:1 };
			this._colorCount++;

			return true;

		}

		return false;

	}

	mean() {

		let rTot = 0, gTot = 0, bTot = 0;

		let count, o,c;

		for( let p in this._colors ) {
	
			o = this._colors[p];

			c = o.color;
			count = o.count;

			//console.log('rtot: ' + rTot + ' c.r: ' + c.r + ' x count: ' + count );
			rTot += (count*c.r);
			gTot += (count*c.g);
			bTot += (count*c.b);

		}

		//rTot = Math.round( rTot / this._pixelCount );
		//gTot = Math.round( gTot / this._pixelCount );
		//bTot = Math.round( bTot / this._pixelCount );
		//console.log('SET MEAN: ' + rTot + ',' + gTot +',' + bTot);

		return new Color( Math.round( rTot / this._pixelCount ),
			Math.round( gTot / this._pixelCount ),
			Math.round( bTot / this._pixelCount ) );
 
	}

	mode() {

		let amt = 0;
		let col = null;

		let o;
		for( let p in this._colors ) {
	
			o = this._colors[p];

			if ( o.count > amt ) {
				amt = o.count;
				col = o.color;
			}

		}

		if ( col !== null ) return color.clone();

	}


}