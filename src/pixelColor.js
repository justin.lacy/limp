export default class PixelColor {

	get color() { return this._color; }
	set color(v) { this._color = v; }

	get loc() { return this._loc; }
	set loc(v) { this._loc = v; }

	constructor( color, loc=null ){

		this._color = color;
		this._loc = loc;

	}

};