const path = require( 'path' );
// loads .vue files.
const VueLoader = require('vue-loader/lib/plugin' );

module.exports = {

	mode:"development",
	entry:{
		limper:"./src/index.js"
	},
	module:{

		rules:[
			{
				test:/\.vue$/,
				loader:'vue-loader'
			}
		]

	},
	plugins:[ new VueLoader()],
	output:{

		path:path.resolve( __dirname, "dist"),
		filename:"[name].dev.bundle.js",
		library:"Limper"
	},
	resolve:{
		modules:[
			path.resolve( __dirname, "src"),
			"node_modules"
		],

		alias:{
			'config':'config_dev',
			'data':'data',
			'view':'view',
			'limp':'limp'
		}
	}

};